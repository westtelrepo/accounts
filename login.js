$(document).ready(function() {
	var $passwd = $('#input_password');
	try {
		// we try and change the type of input. If we can't (as in
		// older versions of IE) then we don't display the "Show
		// Password" option
		// TODO: input replacement in old IE?
		$passwd.attr('type', 'text');
	} catch(e) {
		// we can't do it dynamically. Bail
		return;
	}
	if ($passwd.attr('type') === 'text') {
		$('#show_row').removeClass('display_none');
		$passwd.attr('type', 'password');
		var $show_password = $('#show_password');
		$show_password.prop('checked', false);
		$show_password.change(function() {
			if (this.checked)
				$passwd.attr('type', 'text');
			else
				$passwd.attr('type', 'password');
		});
	}
});
