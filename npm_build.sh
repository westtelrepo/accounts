#!/bin/bash
UGLIFY='uglifyjs'

set -e

mkdir -p static_build/js

# -r (--require) adds as if require('') was used
browserify browser_common.js | $UGLIFY bower_components/jquery-1/dist/jquery.js - node_modules/westtel-utils/mainLayout.js -c --support-ie8 > static_build/js/common.min.js
# -x (--external) tells it that the module will be provided elsewhere (e.g. by above)
# browserify browser_editor.js | uglifyjs - -c > static_build/js/editor.min.js
browserify login.js | $UGLIFY - -c --support-ie8 > static_build/js/login.min.js
browserify change_password | $UGLIFY - -c --support-ie8 > static_build/js/change_password.min.js
browserify main.js | $UGLIFY - -c --support-ie8 > static_build/js/main.min.js
