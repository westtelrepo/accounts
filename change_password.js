$(function() {
	var $pw = $('.password_field');
	try {
		$pw.attr('type', 'text');
	} catch(e) {
		return;
	}

	if ($pw.attr('type') === 'text') {
		$pw.attr('type', 'password');
		var $show_line = $('#show_password_line');
		$show_line.removeClass('display_none');
		var $show = $('#show_password');
		$show.prop('checked', false);
		$show.change(function() {
			if (this.checked)
				$pw.attr('type', 'text');
			else
				$pw.attr('type', 'password');
		});
	}
});
