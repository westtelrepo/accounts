'use strict';
require('systemd');
const Handlebars = require('handlebars');
const assert = require('assert');
const bcrypt = require('bcrypt');
const body_parser = require('body-parser');
const co = require('co');
const co_express = require('co-express');
const csurf = require('csurf');
const express = require('express');
const fs = require('fs');
const helmet = require('helmet');
const moment = require('moment-timezone');
const pgp = require('pg-promise')();
const url = require('url');

const westtel_sessions = require('westtel-utils/sessions');
const westtel_db = require('westtel-utils/db')(pgp);

const express_handlebars = require('express-handlebars').create({
	handlebars: Handlebars,
	defaultLayout: 'main'
});

const app = express();
app.engine('handlebars', express_handlebars.engine);
app.set('port', process.env.PORT || 3002);
app.set('trust proxy', true);
app.set('view engine', 'handlebars');

app.get('/ping', function(req, res) {
	res.send('hello, world from WestTel Accounts!');
});
app.use('/dev/null', function(req, res) {
	res.type('text');
	res.send();
});

app.use(helmet({
	// we don't use iframe
	frameguard:{action:'deny'},

	// this sends a header that browsers can use to whitelist the resources
	// a page is using for security purposes.
	// Only relatively new browsers support this, but it's a good security
	// measure that doesn't hurt older browsers.
	contentSecurityPolicy: {
		directives: {
			baseUri: ["'self'"],

			// best security is default DENY (i.e. whitelist)
			defaultSrc: ["'none'"],

			// only trust scripts from us. Maybe we should have
			// a js.westtel.com for hosting common/all scripts,
			// but for now just keep to this
			//
			// notice that this denies use of eval and inline
			// scripts (so we can't use onload="", inline <script>,
			// etc)
			scriptSrc: ["'self'"],

			// allow google fonts for Droid Sans
			// notably this disables inline styles (style="")
			styleSrc: ["'self'", 'https://fonts.googleapis.com'],

			// jQuery UI uses data URIs
			imgSrc: ["'self'", 'data:'],

			// expand this if we want to use
			// AJAX on the login page or elsewhere
			connectSrc: ["'none'"],

			// i.e. google fonts for Droid Sans
			fontSrc: ['https://fonts.gstatic.com'],

			// we don't use <object>, <embed>, <applet> etc
			objectSrc: ["'none'"],
			// plugin-types is redundant with object-src 'none'
			// pluginTypes: ["'none'"]

			// we don't use <audio> or <video>
			mediaSrc: ["'none'"],

			// we don't use AJAX right now so we can do this,
			// otherwise we need to either implement CORS or
			// add 'allow-same-origin'; however having both
			// 'allow-same-origin' and 'allow-scripts' might not
			// be more secure than not having sandbox directive at
			// all
			sandbox: ['allow-forms', 'allow-scripts'],

			// change this if we want to keep reports, though
			// there is no knowing if they are faked
			reportUri: ['/dev/null'],

			// we don't use iframe
			childSrc: ["'none'"],

			// this doesn't just control where we can POST to;
			// it also restricts redirection after a POST.
			// so we allow all of westtel to be the target of
			// our forms (because they are, indirectly)
			formAction: ["https://*.westtel.com"],

			// we don't use iframe or allow others to do so
			frameAncestors: ["'none'"],
		},
		// let the browser enforce these rules without mercy
		reportOnly: false
	},
	hsts: {
		maxAge: 15768000000,
		force: true,
	},
	referrerPolicy: {
		policy: 'no-referrer'
	}
}));

// we install static resources before session handling. This is on purpose.
app.use('/static', express.static('static', {maxAge:'1 hour'}));
app.use(express.static('static_build', {maxAge:'1 hour'}));
app.use('/static/jquery-ui-1.11.4', express.static('bower_components/jquery-ui-1.11', {maxAge: '1 hour'}));
app.use('/css/mainLayout.css', express.static('node_modules/westtel-utils/mainLayout.css', {maxAge: '1 hour'}));

const db = pgp({});
westtel_sessions.install_sessions(app, {pruneSessionInterval:60, db:db});
if (!app.locals.mainLayout) app.locals.mainLayout = {};
app.locals.mainLayout.title = 'WestTel PSAP Accounts';
app.locals.mainLayout.headHTML = [
`<script type="text/javascript" src="/js/common.min.js"></script>`,
`<!--[if gte IE 8]><!-->
<script src="/static/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/static/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<!--<![endif]-->`,
`<link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>`,
`<link rel="stylesheet" href="/css/mainLayout.css" />`
];

app.use(body_parser.urlencoded({extended:false}));

// CSRF protection
app.use(csurf({cookie:{secure:true,httpOnly:true}}));
app.use(function(req, res, next) {
	res.locals.csrfToken = req.csrfToken();
	next();
});

// this returns a (mostly?) canonical list of time zone names
// based off of https://github.com/moment/moment-timezone/issues/227#issuecomment-194932242
// TODO: use better solution once they come up with one

const tz_list = Object.keys(moment.tz._zones)
	.map(k => moment.tz._zones[k].split('|')[0])
	.filter(z => {
		if (/\//.test(z) && !/^Etc\//.test(z)) return true;
		else return false;
	})
	.sort();

function get_tz_list(current) {
	return tz_list.map(x=>({val:x,selected:current === x}));
}

app.get('/', westtel_sessions.check_auth({}), function(req, res) {
	res.render('hello', {tz: get_tz_list(res.locals.user.timezone)});
});

app.post('/', westtel_sessions.check_auth({}), co_express(function*(req, res) {
	const set = co.wrap(function*(t) {
		yield t.none(`UPDATE users SET timezone = $(tz), disp_name = $(disp_name)
		       WHERE uid = $(uid)`,
			Object.assign(req.body, {uid:res.locals.user.uid}));
	});
	set.txMode = westtel_db.tmSerializableRW;
	yield westtel_db.transactor(db, set);

	res.locals.user.timezone = req.body.tz;
	res.locals.user.disp_name = req.body.disp_name;

	res.render('hello', {message:'Successfully changed settings', tz: get_tz_list(res.locals.user.timezone)});
}));

const URL = require('url');
app.get('/login', function(req, res) {
	res.render('login');
});

// we only allow to redirect to urls that use SSL,
// have no basic auth information (username:password@host),
// have the correct port (absent/443),
// and, most importantly, go to a westtel.com subdomain.
// notice that redirects to westtel.com itself are not allowed.
function allow_redirect(url) {
	let u;
	try {
		u = URL.parse(url);
	} catch(e) {
		return false;
	}

	if (u.protocol !== 'https:')
		return false;
	if (u.auth !== null)
		return false;
	if (u.port !== null && u.port !== '443')
		return false;
	if (!/\.westtel\.com$/.test(u.hostname))
		return false;
	return true;
}

const validate_user_password = co.wrap(function*(user, password) {
	function* get(t) {
		if (user.uid) {
			return {
				uid: user.uid,
				bcrypt: (yield t.oneOrNone('SELECT bcrypt FROM users WHERE uid = $(uid)', user)).bcrypt,
			};
		} else
			return yield t.oneOrNone('SELECT uid, bcrypt FROM users WHERE username = $(username)', user);
	}
	get.txMode = westtel_db.tmSerializableRODeferrable;
	const data = yield westtel_db.transactor(db, get);
	if (data && data.bcrypt) {
		const is_correct = yield new Promise((resolve, reject) =>
			bcrypt.compare(password, data.bcrypt, (err, res) =>
				err ? reject(err) : resolve(res)
			)
		);
		if (is_correct)
			return {uid: data.uid};
		else
			return undefined;
	} else
		return undefined;
});

app.post('/login', co_express(function*(req, res) {
	const username = req.body.uname;
	const password = req.body.passwd;

	if (username === undefined || username === '' || password === undefined || password === '') {
		res.render('login', { error_message: 'Invalid username and/or password' });
		return;
	}

	const timeout_promise = new Promise(resolve => setTimeout(resolve, 1500));

	function* get(t) {
		return yield t.oneOrNone('SELECT uid, bcrypt FROM users WHERE username = $(username)', {username});
	}
	get.txMode = westtel_db.tmSerializableRODeferrable;

	const data = yield westtel_db.transactor(db, get);
	let is_correct = false;

	if (data && data.bcrypt)
		is_correct = yield new Promise((resolve, reject) => 
			bcrypt.compare(password, data.bcrypt, (err, res) =>
				err ? reject(err) : resolve(res)
			)
		);

	if (is_correct) {
		req.session.user = {uid: data.uid};
		res.locals.user = {uid: data.uid, username: username};
		console.log(`[${moment.tz('UTC').format()}] User ${res.locals.user.username} (${res.locals.user.uid}) logged in`);
		// TODO: log

		// we forcibly save the session before redirect because of
		// a timing issue, where the other page reads the session
		// before we write it and we get redirected back here,
		// which we don't want
		yield new Promise((resolve,reject) =>
			req.session.save(err =>
				err ? reject(err) : resolve()
			)
		);
		if (allow_redirect(req.query.redirect)) {
			res.redirect(req.query.redirect);
		} else
			res.redirect('/');
	} else {
		// we wait 1.5 seconds since BEFORE we hash the password to identify it
		// 1) makes guessing harder if they have to wait 1.5 seconds per guess
		// 2) makes a theoretical timing attack on bcrypt (or whatever)
		//    more difficult to execute, and more difficult to guess usernames
		//    from how quickly our call to the database returns (binary trees
		//    are not constant time datastructures, and timing it could allow
		//    a binary search to discover usernames even without passwords
		//    or a "invalid password but not invalid username" message)
		yield timeout_promise;
		res.render('login', { error_message: 'Invalid username and/or password' });
	}
}));

app.get('/change_password', westtel_sessions.check_auth({no_change_password_redirect:true}), co_express(function*(req, res) {
	res.render('change_password', {message: res.locals.user.force_password_change ? 'Please change your password' : undefined});
}));

app.post('/change_password', westtel_sessions.check_auth({no_change_password_redirect:true}), co_express(function*(req, res) {
	if (req.body.new !== req.body.new_again)
		res.render('change_password', {error:'New password fields do not match'});
	else {
		if (yield validate_user_password(res.locals.user, req.body.old)) {
			if (req.body.new === req.body.old) {
				res.render('change_password', {error:'Can not set new password to be the same as old password'});
				return;
			} else if (req.body.new.length < 6) {
				res.render('change_password', {error:'New password too short'});
				return;
			}

			// create new hash (TODO: config rounds?)
			const hash = yield new Promise((resolve, reject) => {
				bcrypt.hash(req.body.new, 10, (err, hash) =>
					err ? reject(err) : resolve(hash)
				)
			});
			function* set(t) {
				// PostgreSQL 9.5 will have built in functions for this
				// for now, just create a quick little function to do this
				t.none(
`CREATE OR REPLACE FUNCTION pg_temp.yydVuAPpGgJuIoZb(val JSONB) RETURNS jsonb LANGUAGE plv8 AS $$
val.force_password_change = undefined; return val; $$`);
				t.none('UPDATE users SET bcrypt = $(hash) WHERE uid = $(uid)',
				       {uid: res.locals.user.uid, hash});
				t.none('UPDATE users SET data = pg_temp.yydVuAPpGgJuIoZb(data) WHERE uid = $1', res.locals.user.uid);
			}
			set.txMode = westtel_db.tmSerializableRW;
			yield westtel_db.transactor(db, set);
			console.log(`[${moment.tz('UTC').format()}] User ${res.locals.user.username} (${res.locals.user.uid}) changed their password`);
			res.render('change_password', {message:'Password successfully changed'});
		} else
			res.render('change_password', {error:'Old password is invalid'});
	}
}));

app.get('/logout', westtel_sessions.check_auth({no_change_password_redirect:true}), co_express(function*(req, res) {
	console.log(`[${moment.tz('UTC').format()}] User ${res.locals.user.username} (${res.locals.user.uid}) logged out`);
	const timeout_promise = new Promise(resolve => setTimeout(resolve, 1500));
	yield new Promise((resolve, reject) =>
		req.session.destroy(err =>
			err ? reject(err) : resolve()
		)
	);
	res.redirect('/login');
}));

app.get('/reset', westtel_sessions.check_auth({superadmin:true}), co_express(function*(req, res) {
	const usernames = yield db.any('SELECT username FROM users ORDER BY 1');
	res.render('reset', {users: usernames});
}));


app.post('/reset', westtel_sessions.check_auth({superadmin:true}), co_express(function*(req, res) {
	const usernames = yield db.any('SELECT username FROM users ORDER BY 1');
	if (!(yield validate_user_password(res.locals.user, req.body.super))) {
		res.render('reset', {users: usernames, message: 'invalid super password'});
		return;
	}

	let hash;
	if (req.body.password)
		hash = yield new Promise((resolve, reject) => {
			bcrypt.hash(req.body.password, 10, (err, hash) =>
				err ? reject(err) : resolve(hash)
			)
		});

	function* do_thing(t) {
		yield t.none(
`CREATE OR REPLACE FUNCTION pg_temp.f(x JSONB) RETURNS JSONB LANGUAGE plv8 AS $$
x.force_password_change = true;
return x;
$$`);
		if (hash)
			yield t.none('UPDATE users SET bcrypt = $(hash) WHERE username = $(username)',
			       {username: req.body.uname, hash});
		if (req.body.force)
			yield t.none('UPDATE users SET data = pg_temp.f(data) WHERE username = $(username)',
			       {username: req.body.uname});
		yield t.none('DROP FUNCTION pg_temp.f(JSONB)');
	};
	do_thing.txMode = westtel_db.tmSerializableRW;
	yield westtel_db.transactor(db, do_thing);
	console.log(`[${moment.tz('UTC').format()}] Super-User ${res.locals.user.username} (${res.locals.user.uid}) changed password details for ${req.body.uname}`);
	res.render('reset', {users: usernames, message:'Password and/or force set'});
}));

app.use(function(req, res) {
	res.sendStatus(404);
});

app.use(function(err, req, res, next) {
	console.log(`Error: ${JSON.stringify(err,null,'\t')}`);
	if (res.headersSent) {
		next(err);
		return;
	} else if (err.code === 'EBADCSRFTOKEN') {
		if (req.session.user !== undefined)
			console.log('csrf_failure', {req:req,res:res});
		res.redirect('/');
	}
	console.error(err.stack);
	res.sendStatus(500); // TODO: better error page
});
const http = require('http');
const server = http.createServer(app).listen(app.get('port'));

process.on('SIGTERM', function() {
	console.log('received SIGTERM');
	server.close(function() {
		console.log('exiting...');
		process.exit();
	})
});
